#!/bin/bash

# this needs FFMPEG compiled up for the Pi with the Pi related stuff

# does a two pass encode to mp4 passing the ac3 audio through and transcoding
# the mpeg2 to h264

# want the vobs and things on a network drive ideally since the SD card is so slow.
# not that the network is much faster...

# use find/exec to transcode all vobs 'somewhere' and output them to the
# same location as mp4s

# the omx stuff doesn't have many options you control quality mostly through
# the -b:v flag
# https://www.lighterra.com/papers/videoencodingh264/ this page is roughly where
# i've taken my values from

# you'll need the mpeg2 license from the pi foundation to use the mpeg2_mmal decoder

find /some-location-with-vobs -maxdepth 1 -type f -iname '*.vob' -exec bash -c '

        ffmpeg -y -an -c:v mpeg2_mmal -i $0 -c:v h264_omx -profile:v high \
               -pass 1 -b:v 2176k -f mp4 /dev/null;

        ffmpeg -c:v mpeg2_mmal -i $0 -c:a copy \
               -c:v h264_omx -profile:v high -b:v 2176k \
               -pass 2 $0.mp4;

' {} \;

