#!/bin/bash

# TODO: check this against server

# many variations on this, vobcopy doesn't do a great job, in my opinion
# since it always seems to just make massive files of not what i want.
# however, its -I option to figure out what is on a disc is very useful

# run that first to work out where things are, not very automateable sadly
# so this needs modifying per disc

# these are the title numbers you are interested in
titles=(2 4 6 8 10)

# number is how many titles are in 'titles' - so you can number them properly
for number in {1..5}; do
    # map the number to the title
    position=${titles[number]}

    # mplayer dump the stream into a vob
    mplayer dvd://${position} -dumpstream -dumpfile $position.vob

done;