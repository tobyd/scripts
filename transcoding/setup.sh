#!/bin/bash

# Install the required packages for debian based systems

# update system
sudo apt-get update && sudo apt-get upgrade -y

# install packages for DVD / Audio manipulation
sudo apt-get install vobcopy mplayer mediainfo libdvd-pkg cdparanoia abcde id3 id3v2 cd-discid

# note on ffmpeg, install this as well if you don't need to compile it up yourself (for a pi for instance)

# enable CSS for DVDs, if legal of course
# sudo dpkg-reconfigure libdvd-pkg