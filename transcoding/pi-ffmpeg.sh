#!/bin/bash

# Setup and Build FFMPEG for the Pi, includes the openmax stuff

# Update the system
sudo apt-get update && sudo apt-get upgrade -y

# Fetch dependencies
sudo apt-get install autoconf automake build-essential libass-dev libfreetype6-dev \
                    libsdl1.2-dev libtheora-dev libtool libva-dev libvdpau-dev libvorbis-dev libxcb1-dev libxcb-shm0-dev \
                    libxcb-xfixes0-dev pkg-config texinfo zlib1g-dev git

# create a location for it to live
mkdir ~/src
cd ~/src

# grab the source
git clone https://github.com/FFmpeg/FFmpeg.git
cd ffmpeg

# checkout the latest stable (you'll need to look at the site for this and get the tag)
git checkout n4.1

# build it with the raspberry pi openmax support
./configure --enable-gpl --enable-nonfree --enable-mmal --enable-omx --enable-omx-rpi

build -j4

# install it
sudo make install